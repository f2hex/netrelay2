#!/usr/bin/env python3

# program used to test Arduino netrelay2

from datetime import datetime, timezone, timedelta
import random, requests, json, time


url = "http://192.168.2.145"
CHANNELS=8

# set channel on

for chan in range(1, CHANNELS+1):
    req_str =f"{url}/set?channel={chan}&state=on"
    print(req_str)
    res = requests.get(req_str)
    print(f"{res.status_code}: {res.text}")
    print(res.text)

for chan in range(1, CHANNELS+1):
    req_str =f"{url}/set?channel={chan}&state=off"
    print(req_str)
    res = requests.get(req_str)
    print(f"{res.status_code}: {res.text}")
    print(res.text)

req_str =f"{url}/set?channel=*&state=on"
print(req_str)
res = requests.get(req_str)
print(f"{res.status_code}: {res.text}")
print(res.text)

req_str =f"{url}/set?channel=*&state=off"
print(req_str)
res = requests.get(req_str)
print(f"{res.status_code}: {res.text}")
print(res.text)

for pin in range(1,CHANNELS-1):
    now = datetime.now(timezone.utc) + timedelta(seconds=random.randrange(150,5000))
    # leading zeros are not allowed in cron expressions

    # due to the need to embed the cron expression into a URL and to avoid URL
    # encoding the cron expression uses a very simple encoding scheme:
    # - slash '/' is represented by '_'
    # - space ' ' by ':'
    #
    cexpr = now.strftime(f"0:%-M:%-H:*:*:*")
    duration = random.randrange(5, 5000)
    req_str =f"{url}/cronset?channel={pin}&state=on&cexpr={cexpr}&dur={duration}&repeat=no"
    print(req_str)
    res = requests.get(req_str)
    print(f"{res.status_code}: {res.text}")
    print(res.text)
    time.sleep(1)

res = requests.get(f"{url}/getsched")
print(f"{res.status_code}: {res.text}")
print(res.text)

time.sleep(20)

res = requests.get(f"{url}/clear?channel=*")
print(f"{res.status_code}: {res.text}")
print(res.text)
