
#include <fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <time.h>

#include <rase.h>

#include "devicedata.h"

#define AVAIL_RELAYS   8
#define AVAIL_SENSORS  0
#define API_PORT       8081
#define NTP_SERVER_LEN 32
#define NETDATA_LEN 2014
#define RESP_HDR_LEN 32

#define boolean int
#define delay(v) sleep(2)

/* used to hold configuration data that is stored on the EEPROM */
typedef struct config_data {
    uint8_t ver[2];
    uint8_t mac[6];
    uint8_t ip[4];
    uint8_t gw[4];
    uint8_t dns[4];
    uint8_t netmask[4];
    char ntpsrv[NTP_SERVER_LEN+1];
} ConfigData;

unsigned char pin_state[AVAIL_RELAYS] = { false,false,false,false,false,false,false,false};

ConfigData config;
// used to suspend any active operation
bool oper_suspended = false;

// socket used for listening
static int ssocket;
// connected client socket
static  int cconn;

/*
 * get string represetnation of boolean state
 */
const char *on_off_state(bool state) {
    return state ? "on":"off";
}

/*
 * Generate API result data (very simple JSON formatted string)
 */
void gen_out_data(char *pout_buf, int buflen, int pin, const char *val, bool add_comma, bool digital) {
    //  assemble the json output
    const char *ptype;
    if (digital) {
        ptype = "state";
    }
    else {
        ptype = "value";
    }
    snprintf(pout_buf, buflen, "{\"channel\":%d, \"%s\":\"%s\"}", pin, ptype, val);
    if (add_comma) {
        strcat(pout_buf, ",");
    }
}

/*
 * Set the value (state) of the specific digital pin
 */
void pin_set(int pin, boolean state) {
    // if the operations are suspended do not perform any real actions on pins
    if (!oper_suspended) {
        // set the pin for output
        DBG_PRINTF2("pin_set: pin=%d state=%d\n", pin, state);
        pin++;
        pin_state[pin] = state;
    }
}

/*
 * Set all the digital I/O pin to the specified state
 */
static void set_digital_pins(bool state) {
    int nx;
    for (nx=1; nx <= AVAIL_RELAYS; nx++) {
        pin_set(nx, state);
        delay(150);
    }
}


void factory_reset(void) {

    /* Setting the default config record
     *
     * **Warning**: Ethernet shield MAC adress
     * The MAC address should be chosen with care just to avoid any clash with existing devices as well as respecting the rules for
     * this (see: https://en.wikipedia.org/wiki/MAC_address#Address_details). In particular the U/L bit, short for Universal/Local,
     * the second-least-significant bit of the first octet of the address, which identifies how the address is administered; if the
     * bit is 0, the address is universally administered, which is why this bit is 0 in all UAAs. If it is 1, the address is locally
     * administered.
     *
     * For example in the address 06-00-00-00-00-00 the first octet is 06 (hexadecimal), the binary form of which is 00000110, where
     * the second-least-significant bit is 1. Therefore, it is a locally administered address and suitable for the use in this
     * project - you need of course to check in your LAN for any possible device that could have the same MAC address.
     *
     * Another important aspect is the unicast/multicast bit that should be set to unicast in order to allow some UDP services to work correctly (like DNS).
     */

    uint8_t ver[] = { VER_MAJOR,VER_MINOR };
    uint8_t mac[] = DEF_MACADDRESS;
    uint8_t ip[] = DEF_IPADDRESS;
    uint8_t gw[] = DEF_GATEWAY;
    uint8_t dns[] = DEF_DNS_SERVER;
    uint8_t netmask[] = DEF_NETMASK;
    const char *ntpsrv = DEF_NTPSERVER;

    memcpy(config.ver,ver, 2);
    memcpy(config.mac, mac,6);
    memcpy(config.ip, ip, 4);
    memcpy(config.gw, gw,4);
    memcpy(config.dns, dns, 4);
    memcpy(config.netmask, netmask, 4);
    strncpy((char *) config.ntpsrv, ntpsrv, NTP_SERVER_LEN);

}

void show_config(void) {
    DBG_PRINTF6("mac: %02x:%02x:%02x:%02x:%02x:%02x\n", config.mac[0], config.mac[1], config.mac[2], config.mac[3], config.mac[4], config.mac[5]);
    DBG_PRINTF4("ip: %d.%d.%d.%d\n", config.ip[0], config.ip[1], config.ip[2], config.ip[3]);
    DBG_PRINTF4("gw: %d.%d.%d.%d\n", config.gw[0], config.gw[1], config.gw[2], config.gw[3]);
    DBG_PRINTF4("dns: %d.%d.%d.%d\n", config.dns[0], config.dns[1], config.dns[2], config.dns[3]);
    DBG_PRINTF4("netmask: %d.%d.%d.%d\n", config.netmask[0], config.netmask[1], config.netmask[2], config.netmask[3]);
    DBG_PRINTF1("ntp server: %s\n", config.ntpsrv);
    DBG_PRINTF2("ver: %d.%d\n", config.ver[0], config.ver[1]);
}

void api_info_handler(Rase *prase) {
    time_t rawtime;
    struct tm *ptime;

    time(&rawtime);
    ptime = localtime(&rawtime);

    snprintf(prase->presp, prase->resp_size,
             "{\"ver\": \"%d.%d\", \"build\": \"%s %s\", \"relays\": \"%d\", \"sensors\": \"%d\", \"mac\": \"%02x:%02x:%02x:%02x:%02x:%02x\",",
             VER_MAJOR, VER_MINOR, __DATE__, __TIME__, AVAIL_RELAYS, AVAIL_SENSORS,
             config.mac[0], config.mac[1], config.mac[2], config.mac[3], config.mac[4], config.mac[5]);
    prase->resp_handler(prase->presp);
    snprintf(prase->presp, prase->resp_size,
             "\"ip\": \"%d.%d.%d.%d\", \"gw\": \"%d.%d.%d.%d\", \"dns\": \"%d.%d.%d.%d\", \"netmask\": \"%d.%d.%d.%d\", ",
             config.ip[0], config.ip[1], config.ip[2], config.ip[3],
             config.gw[0], config.gw[1], config.gw[2], config.gw[3],
             config.dns[0], config.dns[1], config.dns[2], config.dns[3],
             config.netmask[0], config.netmask[1], config.netmask[2], config.netmask[3]);
    prase->resp_handler(prase->presp);
    snprintf(prase->presp, prase->resp_size,
             "\"ntp\": \"%s\", \"suspend\": \"%s\", \"datetime\": \"%04d-%02d-%02d %02d:%02d:%02d\" }",
             config.ntpsrv, on_off_state(oper_suspended), ptime->tm_year+1900, ptime->tm_mon+1, ptime->tm_mday, ptime->tm_hour, ptime->tm_min, ptime->tm_sec);
    prase->resp_handler(prase->presp);
    DBG_PRINT("info requested\n");

}

/*
 * 'set' handler
 *
 * example:
 *
 * curl -v http://arduino-netrelay/set/2/on
 */
void api_set_handler(Rase *prase) {

    int pin = rase_get_value_as_int(prase, "channel");
    bool state = rase_get_value_as_bool_alias(prase, "state");

    if (pin == -1) {
        // set the same value to all the pins (relay channels)
        set_digital_pins(state);
    }
    else {
        // set value to the specific single pin (relay channel)
        pin_set(pin, state);
    }
    gen_out_data(prase->presp, NETDATA_LEN+1, pin, on_off_state(state), false, true);
    prase->resp_handler(prase->presp);
    DBG_PRINTF2("set pin %d %d\n", pin, state);
}

void api_cronset_handler(Rase *prase) {
    int pin = rase_get_value_as_int(prase, "channel");
    bool state = rase_get_value_as_bool_alias(prase, "state");
    const char *cexpr = rase_get_value_as_string(prase, "cexpr");
    int duration = rase_get_value_as_int(prase, "dur");
    bool repeat = rase_get_value_as_bool_alias(prase, "repeat");
    DBG_PRINTF5("cronset chan=%d state=%d cexpr=%s duration=%d repeat=%d\n", pin, state, cexpr, duration, repeat);
}

#define PIN_STATE_BUF_LEN 32

void api_get_handler(Rase *prase) {
    char pin_state_buf[PIN_STATE_BUF_LEN+1];
    int nx;
    bool last = false;

    int pin = rase_get_value_as_int(prase, "channel");
    if (pin == -1) {
        // return the state of all the channels
        strcpy(prase->presp, "[");
        for (nx=0; nx<AVAIL_RELAYS; nx++) {
            gen_out_data(prase->presp, NETDATA_LEN+1, nx+1, on_off_state(pin_state[nx+2]), !last, true);
            prase->resp_handler(prase->presp);
            if (nx == AVAIL_RELAYS) { last = true; }
        }
        strcat(prase->presp, "]");
    }
    else {
        gen_out_data(prase->presp, NETDATA_LEN+1, pin, on_off_state(pin_state[pin+1]), false, true);
        prase->resp_handler(prase->presp);
    }
    DBG_PRINTF2("get pin %d state: %s\n", pin, on_off_state(pin_state[pin+1]));
}

void api_aread_handler(Rase *prase) {
    char aval[10] = "";

    int pin = rase_get_value_as_int(prase, "channel");
    snprintf(aval, 10, "%d", 1024);
    gen_out_data(prase->presp, NETDATA_LEN+1, pin, aval, false, false);
    prase->resp_handler(prase->presp);
    DBG_PRINTF1("get analog channel %d value\n", pin);
}

void api_awrite_handler(Rase *prase) {
    int pin = rase_get_value_as_int(prase, "channel");
    const char *pvalue = rase_get_value_as_string(prase, "value");
    gen_out_data(prase->presp, NETDATA_LEN+1, pin, pvalue, false, false);
    prase->resp_handler(prase->presp);
    DBG_PRINTF2("write analog value %s on pin %d value\n", pvalue, pin);
}

void api_getsched_handler(Rase *prase) {
    DBG_PRINT("getsched API called\n");
}

void api_clear_handler(Rase *prase) {
    DBG_PRINT("clear API called\n");
}

typedef enum addr_conv {
    ADDRCONV_MAC = 0,
    ADDRCONV_IP  = 1
} AddrConv;

static void conv_addr(const char *pdata, uint8_t *pdest, AddrConv conv_type) {
    static const char *ipaddr = "%02x:%02x:%02x:%02x:%02x:%02x";
    unsigned int data[6];
    int elen;
    int nx;

    if (conv_type == ADDRCONV_MAC) {
        sscanf(pdata, "%02x:%02x:%02x:%02x:%02x:%02x", &data[0], &data[1], &data[2], &data[3], &data[4], &data[5]);
        elen = 6;
    }
    else if (conv_type == ADDRCONV_IP) {
        sscanf(pdata, "%d.%d.%d.%d", &data[0], &data[1], &data[2], &data[3]);
        elen = 5;
    }
    for (nx=0; nx<elen; nx++ ) {
        *pdest++ = data[nx];
    }

}

void api_config_handler(Rase *prase) {
    unsigned int data[6];
    int nx;

    DBG_PRINT("Config called\n");
    // get all the required pameters
    conv_addr(rase_get_value_as_string(prase, "mac"), config.mac, ADDRCONV_MAC);
    conv_addr(rase_get_value_as_string(prase, "ip"), config.ip, ADDRCONV_IP);
    conv_addr(rase_get_value_as_string(prase, "gw"), config.gw, ADDRCONV_IP);
    conv_addr(rase_get_value_as_string(prase, "dns"), config.dns, ADDRCONV_IP);
    conv_addr(rase_get_value_as_string(prase, "netmask"), config.netmask, ADDRCONV_IP);
    strncpy(config.ntpsrv, rase_get_value_as_string(prase, "ntpserv"), NTP_SERVER_LEN+1);

    //EEPROM.put(0, config);
    show_config();
    DBG_PRINT("New configuration stored\n");
}

void api_suspend_handler(Rase *prase) {
    DBG_PRINT("suspend API called\n");
}


/*
 * callback used to send response data back to client (just to avoid using large output memory buffer)
 *
 * ap: AParser instance pointer
 * pdata: point to the buffer to be sent
 */
void send_response_data(Rase *prase, char *pdata) {
    //client.print(data);
}


#define SA struct sockaddr


int tcp_init(int port) {
    int sockfd, connfd, len;
    struct sockaddr_in servaddr, cli;
    int opt = 1;
    int sopt;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd != -1) {
        sopt = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
        if (sopt >= 0) {
            bzero(&servaddr, sizeof(servaddr));
            // assign IP, PORT
            servaddr.sin_family = AF_INET;
            servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
            servaddr.sin_port = htons(port);

            // Binding newly created socket to given IP and verification
            if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) == 0) {
                // Now server is ready to listen and verification
                if ((listen(sockfd, 5)) == 0) {
                    printf("Server listening...\n");
                }
                else {
                    sockfd = -4;
                }
            }
            else {
                close(sockfd);
                sockfd = -3;
            }
        }
        else {
            sockfd = -2;
        }
    }

    return sockfd;
}



/*
 *
 */
int wait_http_request(int socket, char *data, int *pconn) {
    struct sockaddr_in servaddr, cli;
    socklen_t clen;
    int nleft;
    int len;
    int nr;

    clen = sizeof(cli);
    // Accept the data packet from client and verification
    printf("Server accepting requests...\n");
    *pconn = accept(socket , (SA*)&cli, &clen);
    if (*pconn >= 0) {
        bzero(data, NETDATA_LEN);
        nleft = NETDATA_LEN;
        while (nleft > 0) {
            nr = read(*pconn, data, nleft);
                printf("read %d bytes\n", nr);
            if (nr == -1) {
                printf("error reading data from socket\n");
                exit(1);
            }
            else if (nr == 0) {
                printf("end of reading EOF\n");
                    break;
            }
            nleft -= nr;
            len = NETDATA_LEN - nleft;
            if (data[len-2] == '\r' && data[len-1] == '\n') {
                data[len] = '\0';
                data[len+1] = '!';
                break;
            }
        }
    }
    else {
        len = -1;
    }

    return len;
}

void tcp_term(int sockfd) {
    close(sockfd);
}

void hex_dump(const void* data, size_t size) {
    char ascii[17];
    size_t i, j;
    ascii[16] = '\0';
    for (i = 0; i < size; ++i) {
        printf("%02X ", ((unsigned char*)data)[i]);
        if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
            ascii[i % 16] = ((unsigned char*)data)[i];
        } else {
            ascii[i % 16] = '.';
        }
        if ((i+1) % 8 == 0 || i+1 == size) {
            printf(" ");
            if ((i+1) % 16 == 0) {
                printf("|  %s \n", ascii);
            } else if (i+1 == size) {
                ascii[(i+1) % 16] = '\0';
                if ((i+1) % 16 <= 8) {
                    printf(" ");
                }
                for (j = (i+1) % 16; j < 16; ++j) {
                    printf("   ");
                }
                printf("|  %s \n", ascii);
            }
        }
    }
}




int rase_resp_handler(char *presp) {
    return send(cconn, presp, strlen(presp), 0);
}

int main(int argc, char *argv[]) {
    static BoolAlias balias[] = { { "yes", true }, { "no", false }, { "on", true }, { "off", false }, { "once", true },{ "repeat", false }, NULL };
    static const char *pp1[] = { "channel", "state", "cexpr", "dur", "repeat" };
    static const char *pp2[] = { "channel", "value" };
    static const char *pp3[] = { "mac", "ip", "gw", "dns", "netmask", "ntpserv" };
    static const char *pp4[] = { "state" };
    static APICommand api_table[] = {
        { "info",       "get", 0,  NULL, api_info_handler     },
        { "cronset",    "get", 5,  pp1 , api_cronset_handler  },
        { "set",        "get", 2,  pp1 , api_set_handler      },
        { "get",        "get", 1,  pp1 , api_get_handler      },
        { "aread",      "get", 1,  pp1 , api_aread_handler    },
        { "awrite",     "get", 2,  pp2 , api_awrite_handler   },
        { "getsched",   "get", 0,  NULL, api_getsched_handler },
        { "clear",      "get", 1,  pp1 , api_clear_handler    },
        { "config",     "get", 6,  pp3 , api_config_handler   },
        { "suspend",    "get", 1,  pp4 , api_suspend_handler  },
        { NULL, NULL, 0, NULL, NULL }
    };

    static char data[NETDATA_LEN+1];
    Rase rase;
    int res;
    int dlen;
    char *pdata;

    factory_reset();

    ssocket = tcp_init(API_PORT);
    if (ssocket >= 0) {
        while (true) {
            dlen = wait_http_request(ssocket, data, &cconn);
            if (dlen > 0) {
                hex_dump(data, dlen);
                pdata = strstr(data, "\r\n");
                if (pdata) {
                    *pdata = '\0';
                    rase_init(&rase, api_table, balias, data, NETDATA_LEN+1, rase_resp_handler);
                    res = rase_process_request(&rase, data);
                }
            }
            else {
                printf("Error receiveing data\n");
            }
            close(cconn);
        }
        tcp_term(ssocket);
    }
    else {
        printf("Error initializing TCP network: %d\n", ssocket);
    }

    return res;
}
