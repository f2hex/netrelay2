

    apic


    static APICommand api_table[] = {
        { "info",       "get", 0, NULL, api_info_handler },
        { "set",        "get", 2, { "chan", "state} ", api_set_handler },
        { "cronset",    "get", 5, { "chan", "state", "sched", "dur", "repeat" }, api_cronset_handler },
        { "get",        "get", 1, { "chan" },  api_get_handler },
        { "aread",      "get", 1, { "chan" }, api_aread_handler   },
        { "awrite",     "get", 2, { "chan", "value" }, api_awrite_handler  },
        { "getsched",   "get", 0, NULL, api_get_schedule },
        { "clear",      "get", 1, { "chan"}, api_clear_handler },
        { "config",     "get", 6, { "mac", "ip", "gw", "dns", "netmask", "ntpserv" }, api_config },
        { "suspend",    "get", 1, { "state"}, api_suspend },
        NULL


curl -X 'GET' 'https://192.168.2.1/cronset?chan=7&state=on&sched=0:42:11:*:*:*&dur=8&repeat=once'
Sketch uses 42458 bytes (86%) of program storage space. Maximum is 49152 bytes.
Global variables use 2174 bytes (35%) of dynamic memory, leaving 3970 bytes for local variables. Maximum is 6144 bytes.

ketch uses 43024 bytes (87%) of program storage space. Maximum is 49152 bytes.
Global variables use 2814 bytes (45%) of dynamic memory, leaving 3330 bytes for local variables. Maximum is 6144 bytes.
