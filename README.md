# Netrelay

Arduino based network relay controller

## The software project

This is a software project to use [Arduino nano](https://www.arduino.cc/en/Main/ArduinoBoardNano), equipped with
an Ethernet shield, to control on of more relays, using a relay shield.

The project implement a very basic _pseudo_ REST HTTP server that
implements the following API call commands:

* **info**: retrieve info of the service: API version, instaled relays
* **set**: write a digital value on a specified pin or all pins  on Arduino
* **cronset**: write a digital value on a specified pin on Arduino using a crontab based schedule
* **get**: read the latest digital value set on a specified pin or all pins on Arduino
* **aread**: read from the specified pin on Arduino an analog value
* **awrite**: write an analog value to the specified Arduino pin
* **getsched**: get the current active scheduled operations
* **clear**: clear one or all active scheduled operations
* **config**: store the specified new configuration on EEPROM and perform a hard reset
* **suspend**: enable or disable suspend mode, when suspend in active API calls or scheduled operations have no real effect on device I/O pins
* **reset**: perform a hard reset of the board and optionally reset to factory configuration

The pin number start from 1 and is mapped to pin 2 on Arduino assuming to have up to 8 relays (pin 2 to pin 9)

All the API calls use only the `GET` method to simplify the code in the sketch and make less use of the microcontroller resources.

The syntax for an API call is the following:
```
http:/arduino_ip/cmd[/pin][/value]

```
These are some examples:

 * `http://192.168.2.145/info`: get information about the service
 * `http://192.168.2.145/set?chan=9&state=on`: set pin 9 ON
 * `http://192.168.2.145/set?chan=4&state=off`: set pin 4 OFF
 * `http://192.168.2.145/get?chan=2`: get the last digital value sent to pin 2
 * `curl http://192.168.2.145/cronset?chan=7&state=on&cexpr=0:42:11:*:*:*&dur=6&repeat=no` schedule relay on pin 7 to be on (closed) at 11:42 only one time and to keep it on for 6 seconds
 * `curl http://192.168.2.145/cronset?chan=7&state=on&cexpr-0:0:6:*:*:*&dur=15&repeat=yes` schedule relay on pin 7 to be on (closed) every day at 06:00 and to keep it on for 15 seconds
 * `curl http://192.168.2.145/clear?chan=2` clear pin 3 timer
 * `curl http://192.168.2.145/clear?chan*` clear all the pin timers
 * `curl http://192.168.2.145/getsched` returns the current pin scheduling plan

**note**: the format of the crontab expression is based on the syntax with 6 field (the first field indicates the number of
seconds) due to the need to embed the cron expression into a URL and to avoid to perform URL encoding the cron expression uses a very simple
encoding schema:

* slash '/' is represented by '_'
* space ' ' is represented by ':'

so for example this cron expression `0 25 8 * * *` must be written as `0:25:8:*:*:*` and `0 1/5 0 * * *` like `0:1-5:0:*:*:*`.

## Current implementation

The latest implementation, that includes the cron based scheduler, uses an [Arduino Nano
Every](https://docs.arduino.cc/static/2b10c676ab53d1d575a2da9ff570150f/ABX00028-datasheet.pdf), based on the AVR ATmega4809 with an Ethernet shield (based the
[PIC Microchip ENC28J60](http://www.microchip.com/wwwproducts/en/en022889) and a 4 or 8 relays board plus a power module (5v 200mA).

The original Arduino Nano wih the ATmega328 has not enough memory to hold the code with the crontab scheduler.

The API service provides, through an API call, the information about the version of the software as well the available relays which
can be operated with the REST API calls.

The following image is the first version of _Arduino netrelay_, based on Arduino Nano original:
![image](images/netrelay-board1.png)

while the photo below shows the prototype of the _Arduino netrelay 2_ based on Arduno Nano Every.

![image](images/netrelay2-dev.png)

The software allow to easily add more commands as it has a table driven command dispatcher, to add any additional command it is
sufficient to create an entry in the API command dispatch table (`APICommand api_table[]`), and then specifying the command name,
the number of parameters, the pointer to a list of parameter names and a pointer to a callback function that takes care of the
API command execution. Here is the current table:

```C
static BoolAlias balias[] = { { "yes", true }, { "no", false }, { "on", true }, { "off", false }, { "once", true },{ "repeat", false }, NULL };

static const char *pp1[] = { "chan", "state", "cexpr", "dur", "repeat" };
static const char *pp2[] = { "chan", "value" };
static const char *pp3[] = { "mac", "ip", "gw", "dns", "netmask", "ntpserv" };
static const char *pp4[] = { "state" };

static APICommand api_table[] = {
    { "info",       "get", 0,  NULL, api_info_handler     },
    { "cronset",    "get", 5,  pp1 , api_cronset_handler  },
    { "set",        "get", 2,  pp1 , api_set_handler      },
    { "get",        "get", 1,  pp1 , api_get_handler      },
    { "aread",      "get", 1,  pp1 , api_aread_handler    },
    { "awrite",     "get", 2,  pp2 , api_awrite_handler   },
    { "getsched",   "get", 0,  NULL, api_getsched_handler },
    { "clear",      "get", 1,  pp1 , api_clear_handler    },
    { "config",     "get", 6,  pp3 , api_config_handler   },
    { "suspend",    "get", 1,  pp4 , api_suspend_handler  },
    { NULL, NULL, 0, NULL, NULL }
};
```
The `balias` key/value array it is used to validate API parameter values that need to be translated easily in boolean equivalents.
Here is the code of the handler that implements the `api_cronset_handler`:

```C
/*
 * 'cronset' handler
 *
 * example:
 *
 * curl -v "http://192.168.2.1/cronset?chan=4&state=on&cexpr=0:39:08:*:*:*&dur=20&repeat=no"
 */
void api_cronset_handler(Rase *prase) {
    // get all the required pameters
    int pin = rase_get_value_as_int(prase, "chan");
    bool state = rase_get_value_as_bool_alias(prase, "state");
    char *cexpr = rase_get_value_as_string(prase, "cexpr");
    int duration = rase_get_value_as_int(prase, "dur");
    bool repeat = rase_get_value_as_bool_alias(prase, "repeat");
    char *pc = cexpr;

    while (*pc) {
        if (*pc == ':') {
            *pc = ' ';
        }
        if (*pc == '-') {
            *pc = '/';
        }
        pc++;
    }
    int cid = schedule_cron_operation(pin, cexpr, state, duration, !repeat);
    if (cid == dtINVALID_ALARM_ID) {
        char *error = "cron scheduling error";
        snprintf(prase->presp, NETDATA_LEN+1, "{\"reason\": \"%s\"}", error);
    prase->resp_handler(prase->presp);
        DBG_PRINTF1("%s\n", error);
    }
    else {
        DBG_PRINTF5("set pin %d %d scheduled on '%s' for %d seconds repeat=%s\n", pin, state, cexpr, duration, repeat ? "no":"yes");
    }
}
```

### HTTP API Request processing

The processing of the REST API commands is implemented in a separated library, RASI (RESTful API Service Engine) that performs all
the bulk load of parsing and dispatching callbacks for API commands. Moreover it takes care of handling the responses flow, the API
callback just need to send the specific API payload using JSON.

### Network related configuration

The software uses a default network configuration that it is stored into the EEPROM of the Arduino device using a dedicated layout (structure):

```C
typedef struct config_data {
    uint8_t ver[2];
    uint8_t mac[6];
    uint8_t ip[4];
    uint8_t gw[4];
    uint8_t dns[4];
    uint8_t netmask[4];
    uint8_t ntpsrv[NTP_SERVER_LEN+1];
} ConfigData;
```

The saved configuration can be modified using a dedicated API command, `config` and there is also a factory_default() function that
resets the device factory configuration in case of need. This function can bel called after a physical button, linked to digital
input pin, is pressed for a specific amount of time (for example 5 seconds).

#### Device ethernet MAC address

**Warning**: the MAC address should be chosen with care just to avoid any clash with existing devices as well as respecting the
rules for this (see: https://en.wikipedia.org/wiki/MAC_address#Address_details). In particular the U/L bit, short for
Universal/Local, the second-least-significant bit of the first octet of the address, which identifies how the address is
administered; if the bit is 0, the address is universally administered, which is why this bit is 0 in all UAAs. If it is 1, the
address is locally administered.

For example in the address 06-00-00-00-00-00 the first octet is 06 (hexadecimal), the binary form of which is 00000110, where the
second-least-significant bit is 1. Therefore, it is a locally administered address and suitable for the use in this project - you
need of course to check in your LAN for any possible device that could have the same MAC address.

Another important aspect is the unicast/multicast bit that should be set to unicast in order to allow some UDP services to work
correctly (like DNS). It is important to be certain that the vale of the MAC address has the unicast/multicast bit set to zero.



### required libraries

The core libray for the mega avr is needed, to install it using the `arduino-cli`:

```
arduino-cli core install arduino:megaavr
```

Then two additional libraries are required:

* EthenetENC
* TimeLibs
* Timezone

These can be installed, still using `arduino-cli` the following commands:

'''
arduino-cli lib install EthernetENC
arduino-cli lib install TimeLib
arduino-cli lib install Timezone
'''

if you use platform.io:

```
pio lib install EthernetENC
pio lib install Time
pio lib install Timezone
```

moreover the [CronAlarms](https://github.com/staticlibs/ccronexpr) library (Copyright (c) 2019 Martin Laclaustra) was used to create
a clone of it with changes required for this project, this library included also another C based module, [Cron expression
parser](https://github.com/staticlibs/ccronexpr) (Copyright 2015, alex at staticlibs.net).

The major difference in the custom CronAlarms is the ability to support scheduled on/off command in chain by specifying the interval
between the first scheduled state and the followin change. It is possible, in this way to schedule to close a relay say at 11:30 and
open it again after 30 minutes just with a single schedule.

### compile, upload and monitor the serial console using Arduino CLI

```
acli compile
acli upload -p /dev/ttyUSB0
acli monitor -p /dev/ttyUSB0 -c baudrate=57600
```

### compilation, upload and monitor the serial console using Platform.io

* compile, link and upload the binary to the board: `pio run -v -t upload`
* monitor the serial port: `pio device monitor -b 57600`
