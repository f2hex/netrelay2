#define VER_MAJOR  2
#define VER_MINOR  1

#define DEF_MACADDRESS  { 0x92, 0xA2, 0xDA, 0x0E, 0xFE, 0x40 }
#define DEF_IPADDRESS   { 192,168,2,1 }
#define DEF_GATEWAY     { 192,168,2,253 }
#define DEF_DNS_SERVER  { 1,1,1,1 }
#define DEF_NETMASK     { 255,255,255,0 }
#define DEF_NTPSERVER   "pool.ntp.org"
