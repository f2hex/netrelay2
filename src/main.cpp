/*
 netrelay: Arduino network controlled relay board

 Copyright (c) 2022 Franco Fiorese <franco@f2hex.net>

 first code date: 26-mar-2017
 updated: 12-Sep-2022
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <Arduino.h>

#include <avr/wdt.h>
#include <EEPROM.h>
#include <TimeLib.h>
#include <Timezone.h>
#include <EthernetENC.h>

#define _ARDUINO_ true

#include <debug.h>
#include <rase.h>
#include <cronalarms.h>
#include <ntpclient.h>

#include "devicedata.h"

#define AVAIL_RELAYS     8
#define AVAIL_SENSORS    0
#define AVAIL_RELAYS     8
#define MAX_PINS         14
#define API_PORT         80

// HTTP request/response buffer size
#define NETDATA_LEN      256
#define CRONEXPR_BUFSIZE 24
#define NTP_SERVER_LEN   32
#define NTP_LOCAL_PORT   8888

/* used to convert MAC and IP addresses */
typedef enum addr_conv {
    ADDRCONV_MAC = 0,
    ADDRCONV_IP  = 1
} AddrConv;

/* used to hold configuration data that is stored on the EEPROM */
typedef struct config_data {
    uint8_t ver[2];
    uint8_t mac[6];
    uint8_t ip[4];
    uint8_t gw[4];
    uint8_t dns[4];
    uint8_t netmask[4];
    uint8_t ntpsrv[NTP_SERVER_LEN+1];
} ConfigData;

/* used to control crontab based scheduled pin state change */
typedef struct {
    char cexpr[CRONEXPR_BUFSIZE+1];
    boolean state;
    int duration;
    boolean armed;
    CronID_t cid;
} ScheduleData;

static int schedule_cron_operation(int pin, char *cexpr, boolean state, int duration, bool one_shot);
static int schedule_alarm_operation(int pin, time_t atime, boolean state);

// MAC address for this arduino
ConfigData config;
// used to suspend any active operation
bool oper_suspended = false;
// static IP address
IPAddress static_ip(192, 168, 2, 145);
IPAddress gateway(192, 168, 2, 254);
IPAddress subnet(255, 255, 255, 0);
// ip address of the time server
IPAddress time_server_ip(192,168,2,250);
// connected client instance
EthernetClient client;
// variable to store previous displayed time
time_t prev_display = 0;

unsigned char pin_state[MAX_PINS] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

// table with pin scheduled operation data
ScheduleData sched_data[AVAIL_RELAYS] = {
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID },
    { "", false, 0, false, dtINVALID_ALARM_ID }
};

// RESTful API Server Engine instance
static Rase rase;

// reset control logic vars
static bool reset_req = false;
static int reset_wait = 4;

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
static EthernetServer server(API_PORT);
// buffer used for HTTP request/response processing
static char netbuf[NETDATA_LEN+1];

// Light sensor input pin for LDR (analog)
//#define LIGHT_SENSOR_PIN  A0
// latest value coming from the LDRsensor
//static int light_sensor_val = 0;

//Central European Time (Rome, Frankfurt, Paris)
static TimeChangeRule CEST = { "CEST", Last, Sun, Mar, 2, 120};     //Central European Summer Time
static TimeChangeRule CET = { "CET ", Last, Sun, Oct, 3, 60};       //Central European Standard Time
static Timezone CE(CEST, CET);


/*
 * Set the value (state) of the specific digital pin
 */
void pin_set(int pin, boolean state) {
    // if the operations are suspended do not perform any real actions on pins
    if (!oper_suspended) {
        // set the pin for output
        DBG_PRINTF2("pin_set: pin=%d state=%d\n", pin, state);
        pin++;
        pinMode(pin, OUTPUT);
        digitalWrite(pin, state ? LOW: HIGH);
        pin_state[pin] = state;
    }
}

/*
 * Set all the digital I/O pin to the specified state
 */
static void set_digital_pins(bool state) {
    int nx;
    for (nx=1; nx <= AVAIL_RELAYS; nx++) {
        pin_set(nx, state);
        delay(150);
    }
}


/*
 * Perform a reboot/reset
 */
void software_reset(void) {
    // start watchdog with the provided prescaller
    wdt_enable(WDTO_60MS);
    while(1) {}
}

/*
 * Reset netrelay to the default factory configuration
 *
 * reset: if true perform a reset after restoring the default config
 */
void factory_reset(bool reset) {

    /* Setting the default config record
     *
     * **Warning**: Ethernet shield MAC adress
     * The MAC address should be chosen with care just to avoid any clash with existing devices as well as respecting the rules for
     * this (see: https://en.wikipedia.org/wiki/MAC_address#Address_details). In particular the U/L bit, short for Universal/Local,
     * the second-least-significant bit of the first octet of the address, which identifies how the address is administered; if the
     * bit is 0, the address is universally administered, which is why this bit is 0 in all UAAs. If it is 1, the address is locally
     * administered.
     *
     * For example in the address 06-00-00-00-00-00 the first octet is 06 (hexadecimal), the binary form of which is 00000110, where
     * the second-least-significant bit is 1. Therefore, it is a locally administered address and suitable for the use in this
     * project - you need of course to check in your LAN for any possible device that could have the same MAC address.
     *
     * Another important aspect is the unicast/multicast bit that should be set to unicast in order to allow some UDP services to work correctly (like DNS).
     */

    uint8_t ver[] = { VER_MAJOR,VER_MINOR };
    uint8_t mac[] = DEF_MACADDRESS;
    uint8_t ip[] = DEF_IPADDRESS;
    uint8_t gw[] = DEF_GATEWAY;
    uint8_t dns[] = DEF_DNS_SERVER;
    uint8_t netmask[] = DEF_NETMASK;
    char *ntpsrv = DEF_NTPSERVER;

    memcpy(config.ver,ver, 2);
    memcpy(config.mac, mac,6);
    memcpy(config.ip, ip, 4);
    memcpy(config.gw, gw,4);
    memcpy(config.dns, dns, 4);
    memcpy(config.netmask, netmask, 4);
    strncpy(config.ntpsrv, ntpsrv, NTP_SERVER_LEN);

    EEPROM.put(0, config);
    if (reset) {
        reset_req = true;
    }
}

/*
 * Display the current configuration on the serial channel
 */
void show_config(void) {
    DBG_PRINTF6("mac: %02x:%02x:%02x:%02x:%02x:%02x\n", config.mac[0], config.mac[1], config.mac[2], config.mac[3], config.mac[4], config.mac[5]);
    DBG_PRINTF4("ip: %d.%d.%d.%d\n", config.ip[0], config.ip[1], config.ip[2], config.ip[3]);
    DBG_PRINTF4("gw: %d.%d.%d.%d\n", config.gw[0], config.gw[1], config.gw[2], config.gw[3]);
    DBG_PRINTF4("dns: %d.%d.%d.%d\n", config.dns[0], config.dns[1], config.dns[2], config.dns[3]);
    DBG_PRINTF4("netmask: %d.%d.%d.%d\n", config.netmask[0], config.netmask[1], config.netmask[2], config.netmask[3]);
    DBG_PRINTF1("ntp server: %s\n", config.ntpsrv);
    DBG_PRINTF2("ver: %d.%d\n", config.ver[0], config.ver[1]);
}

/*
 * get string represetnation of boolean state
 */
const char *on_off_state(bool state) {
    return state ? "on":"off";
}

/*
 * Generate API result data (very simple JSON formatted string)
 */
void gen_out_data(char *out_buf, int buflen, int pin, char *val, bool add_comma, bool digital) {
    //  assemble the json output
    char *ptype;
    if (digital) {
        ptype = "state";
    }
    else {
        ptype = "value";
    }
    snprintf(out_buf, buflen, "{\"channel\":%d, \"%s\":\"%s\"}", pin, ptype, val);
    if (add_comma) {
        strcat(out_buf, ",");
    }
}


/*
 * Execute crontab base channel set operations
 */
void cron_handler(int pin) {
    ScheduleData *psd = &sched_data[pin-1];

    if (psd->armed) {
        pin_set(pin, psd->state);
        psd->armed = false;
        psd->cid = dtINVALID_ALARM_ID;
        DBG_PRINTF2("crontab action set pin: %d to: %d\n", pin, psd->state);
        if (psd->duration > 0) {
            //char cexpr[CRONEXPR_BUFSIZE+1];
            //sprintf(cexpr, "0 0/%d * * * *", psd->duration + 1);
            int cid = schedule_alarm_operation(pin, psd->duration, !psd->state);
            DBG_PRINTF1("crontab reset rc=%d\n", cid);
        }
    }
}


/*
 * Schedule a pin state set using a crontab expression for a specific amount of time
 *
 *  pin: the pin to be set when alarm will fire
 *  cexpr: the standard CRON expression (6 fields: includes seconds) to use to program the alarm
 *  duration: how long to wait (seconds) until the state will be reset
 *  state: the state to be set when alarm will fire
 *  one_shot: true if the alarm should fire only one time
 */
int schedule_cron_operation(int pin, char *cexpr, boolean state, int duration, bool one_shot) {
    ScheduleData *psd = &sched_data[pin-1];

    CronID_t cid = Cron.create_cron(cexpr, cron_handler, one_shot, pin);
    if (cid != dtINVALID_ALARM_ID) {
        strncpy(psd->cexpr, cexpr, CRONEXPR_BUFSIZE+1);
        psd->duration = duration;
        psd->state = state;
        psd->armed = true;
        psd->cid = cid;
        DBG_PRINTF4("crontab task id: %d with pin=%d duration=%d repeat=%d\n", cid, pin, duration, !one_shot);
    }
    else {
        DBG_PRINT("Error creating a cron timer\n");
    }
    return cid;
}

/*
 * Schedule a pin state set using a seconds based alarm
 *
 *  pin: the pin to be set when alarm will fire
 *  atime: the alarm time in second from now
 *  state: the state to be set when alarm will fire
 */
int schedule_alarm_operation(int pin, time_t atime, boolean state) {
    ScheduleData *psd = &sched_data[pin-1];

    CronID_t cid = Cron.create_alarm(atime, cron_handler, pin);
    if (cid != dtINVALID_ALARM_ID) {
        psd->duration = 0;
        psd->state = state;
        psd->armed = true;
        psd->cid = cid;
        DBG_PRINTF3("alarm task id: %d with pin=%d duration=%d\n", cid, pin, atime);
    }
    else {
        DBG_PRINT("Error creating an alarm timer\n");
    }
    return cid;
}



/*
 * 'info' handler
 *
 * return the information about the relay board, typically a JSON string like this one:
 *
 * {"ver": "1.1", "relays": "8", "sensors": "0"}
 *
 * example:
 *
 * curl -v http://arduino.netrelay.net/info
 * */
void api_info_handler(Rase *prase) {
    TimeChangeRule *tcr;        //pointer to the time change rule, use to get the TZ abbrev

    time_t lt = CE.toLocal(now(), &tcr);
    snprintf(prase->presp, NETDATA_LEN+1,
             "{\"ver\": \"%d.%d\", \"build\": \"%s %s\", \"relays\": \"%d\", \"sensors\": \"%d\", \"mac\": \"%02x:%02x:%02x:%02x:%02x:%02x\",",
             VER_MAJOR, VER_MINOR, __DATE__, __TIME__, AVAIL_RELAYS, AVAIL_SENSORS,
             config.mac[0], config.mac[1], config.mac[2], config.mac[3], config.mac[4], config.mac[5]);
    prase->resp_handler(prase->presp);
    snprintf(prase->presp, NETDATA_LEN+1,
             "\"ip\": \"%d.%d.%d.%d\", \"gw\": \"%d.%d.%d.%d\", \"dns\": \"%d.%d.%d.%d\", \"netmask\": \"%d.%d.%d.%d\", ",
             config.ip[0], config.ip[1], config.ip[2], config.ip[3],
             config.gw[0], config.gw[1], config.gw[2], config.gw[3],
             config.dns[0], config.dns[1], config.dns[2], config.dns[3],
             config.netmask[0], config.netmask[1], config.netmask[2], config.netmask[3]);
    prase->resp_handler(prase->presp);
    snprintf(prase->presp, NETDATA_LEN+1,
             "\"ntp\": \"%s\", \"suspend\": \"%s\", \"datetime\": \"%04d-%02d-%02d %02d:%02d:%02d\", \"TZ\": \"%s\"}",
             config.ntpsrv, on_off_state(oper_suspended), year(lt), month(lt), day(lt), hour(lt), minute(lt), second(lt), tcr->abbrev);
    prase->resp_handler(prase->presp);
    DBG_PRINT("info requested\n");
}


/*
 * 'set' handler
 *
 * example:
 *
 * curl -v http://arduino-netrelay/set/2/on
 */
void api_set_handler(Rase *prase) {

    int pin = rase_get_value_as_int(prase, "channel");
    bool state = rase_get_value_as_bool_alias(prase, "state");

    if (pin == -1) {
        // set the same value to all the pins (relay channels)
        set_digital_pins(state);
    }
    else {
        // set value to the specific single pin (relay channel)
        pin_set(pin, state);
    }
    gen_out_data(prase->presp, NETDATA_LEN+1, pin, on_off_state(state), false, true);
    prase->resp_handler(prase->presp);
    DBG_PRINTF2("set pin %d %d\n", pin, state);
}

/*
 * 'cronset' handler
 *
 * example:
 *
 * curl -v "http://192.168.2.1/cronset?chan=4&state=on&cexpr=0:39:08:*:*:*&dur=20&repeat=no"
 */
void api_cronset_handler(Rase *prase) {
    // get all the required pameters
    int pin = rase_get_value_as_int(prase, "channel");
    bool state = rase_get_value_as_bool_alias(prase, "state");
    char *cexpr = rase_get_value_as_string(prase, "cexpr");
    int duration = rase_get_value_as_int(prase, "dur");
    bool repeat = rase_get_value_as_bool_alias(prase, "repeat");
    char *pc = cexpr;

    while (*pc) {
        if (*pc == ':') {
            *pc = ' ';
        }
        if (*pc == '_') {
            *pc = '/';
        }
        pc++;
    }
    int cid = schedule_cron_operation(pin, cexpr, state, duration, !repeat);
    if (cid == dtINVALID_ALARM_ID) {
        char *error = "cron scheduling error";
        snprintf(prase->presp, NETDATA_LEN+1, "{\"reason\": \"%s\"}", error);
        prase->resp_handler(prase->presp);
        DBG_PRINTF1("%s\n", error);
    }
    else {
        DBG_PRINTF5("set pin %d %d scheduled on '%s' for %d seconds repeat=%s\n", pin, state, cexpr, duration, repeat ? "no":"yes");
    }
}

/*
 * 'get' handler
 *
 * example:
 *
 * curl -v http://arduino-netrelay/get/2
 */
#define PIN_STATE_BUF_LEN 32

void api_get_handler(Rase *prase) {
    char pin_state_buf[PIN_STATE_BUF_LEN+1];
    int nx;
    bool last = false;

    int pin = rase_get_value_as_int(prase, "channel");
    if (pin == -1) {
        // return the state of all the channels
        strcpy(prase->presp, "[");
        for (nx=0; nx<AVAIL_RELAYS; nx++) {
            gen_out_data(prase->presp, NETDATA_LEN+1, nx+1, on_off_state(pin_state[nx+2]), !last, true);
            prase->resp_handler(prase->presp);
            if (nx == AVAIL_RELAYS) { last = true; }
        }
        strcat(prase->presp, "]");
    }
    else {
        gen_out_data(prase->presp, NETDATA_LEN+1, pin, on_off_state(pin_state[pin+1]), false, true);
        prase->resp_handler(prase->presp);
    }
    DBG_PRINTF2("get pin %d state: %s\n", pin, on_off_state(pin_state[pin+1]));
}

/*
 * 'aread' handler
 *
 * example:
 *
 * curl -v http://arduino-netrelay/aread/8
 */
void api_aread_handler(Rase *prase) {
    char aval[10] = "";

    int pin = rase_get_value_as_int(prase, "channel");
    snprintf(aval, 10, "%d", analogRead(pin));
    gen_out_data(prase->presp, NETDATA_LEN+1, pin, aval, false, false);
    prase->resp_handler(prase->presp);
    DBG_PRINTF1("get analog pin %d value\n", pin);
}

/*
 * 'awrite' handler
 *
 * example:
 *
 * curl -v http://arduino-netrelay/awrite/8/120
 */

void api_awrite_handler(Rase *prase) {
    int pin = rase_get_value_as_int(prase, "channel");
    int val = rase_get_value_as_int(prase, "value");
    pinMode(pin, OUTPUT);
    analogWrite(pin, atoi(val));
    gen_out_data(prase->presp, NETDATA_LEN+1, pin, val, false, false);
    prase->resp_handler(prase->presp);
    DBG_PRINTF2("write analog value %d on pin %d value\n", val, pin);
}
/*
 * 'clear' handler
 *
 * example:
 *
 * curl -v http://arduino-netrelay/awrite/8/120
 */

void api_clear_handler(Rase *prase) {
    int xp;

    int pin = rase_get_value_as_int(prase, "channel");
    if (pin == -1) {
        // set the same value to all the pins (relay channels)
        ScheduleData *psd = &sched_data[0];
        for (xp=0; xp < AVAIL_RELAYS; xp++) {
            if (sched_data[xp].armed) {
                Cron.free(sched_data[xp].cid);
            }
            sched_data[xp].armed = false;
            sched_data[xp].cexpr[0] = '\0';
            sched_data[xp].cid = dtINVALID_ALARM_ID;
            DBG_PRINTF1("timer cleared for pin: %d\n", xp+1);
        }
    }
    else {
        // set value to the specific single pin (relay channel)
        if (sched_data[pin-1].armed) {
            Cron.free(sched_data[pin-1].cid);
        }
        sched_data[pin-1].armed = false;
        sched_data[pin-1].cexpr[0] = '\0';
        sched_data[pin-1].cid = dtINVALID_ALARM_ID;
        DBG_PRINTF1("timer cleared for pin: %d\n", pin);
    }
    prase->resp_handler("[]");
}

#define SCHED_STATE_BUF_LEN 96

void api_getsched_handler(Rase *prase) {
    int nx;
    int count = 0;

    // return the state of all the channels
    prase->resp_handler("[");
    for (nx=0; nx<AVAIL_RELAYS; nx++) {
        if (sched_data[nx].armed) {
            count++;
        }
    }
    for (nx=0; nx<AVAIL_RELAYS; nx++) {
        if (sched_data[nx].armed) {
            snprintf(prase->presp, NETDATA_LEN+1, "{\"pin:\": %d, \"cexpr\": \"%s\", \"state\": \"%s\", \"duration\": %d}",
                     nx+1, sched_data[nx].cexpr, on_off_state(sched_data[nx].state), sched_data[nx].duration);
            count--;
            if (count) {
                strcat(prase->presp, ",");
            }
            prase->resp_handler(prase->presp);
        }
    }
    prase->resp_handler("]");
    DBG_PRINT("scheduling state report\n");
}

static void conv_addr(const char *pdata, uint8_t *pdest, AddrConv conv_type) {
    static const char *ipaddr = "%02x:%02x:%02x:%02x:%02x:%02x";
    unsigned int data[6];
    int elen;
    int nx;

    if (conv_type == ADDRCONV_MAC) {
        sscanf(pdata, "%02x:%02x:%02x:%02x:%02x:%02x", &data[0], &data[1], &data[2], &data[3], &data[4], &data[5]);
        elen = 6;
    }
    else if (conv_type == ADDRCONV_IP) {
        sscanf(pdata, "%d.%d.%d.%d", &data[0], &data[1], &data[2], &data[3]);
        elen = 5;
    }
    for (nx=0; nx<elen; nx++ ) {
        *pdest++ = data[nx];
    }

}

// API command to store a new configuration
// curl -v "http://192.168.2.1/config?mac=92:a2:da:0e:fe:40&ip=192.168.2.145&gw=192.168.2.253&dns=192.168.2.250&netmask=255.255.255.0&ntpserv=pool.ntp.org"
void api_config_handler(Rase *prase) {

    // get all the required pameters
    conv_addr(rase_get_value_as_string(prase, "mac"), config.mac, ADDRCONV_MAC);
    conv_addr(rase_get_value_as_string(prase, "ip"), config.ip, ADDRCONV_IP);
    conv_addr(rase_get_value_as_string(prase, "gw"), config.gw, ADDRCONV_IP);
    conv_addr(rase_get_value_as_string(prase, "dns"), config.dns, ADDRCONV_IP);
    conv_addr(rase_get_value_as_string(prase, "netmask"), config.netmask, ADDRCONV_IP);
    strncpy(config.ntpsrv, rase_get_value_as_string(prase, "ntpserv"), NTP_SERVER_LEN+1);
    EEPROM.put(0, config);
    show_config();
    DBG_PRINT("New configuration stored\nPerform a reboot now\n");
    reset_req = true;
    prase->resp_handler("{}");
}

/*
 * Suspend or Resume any I/O operation - when suspend is active any I/O operation is ignored
 *
 * curl -v "http://192.168.2.1/suspend&state=on"
 */
void api_suspend_handler(Rase *prase, char *resp_body) {
    oper_suspended = rase_get_value_as_bool_alias(prase, "state");
    snprintf(prase->presp, NETDATA_LEN+1, "{\"suspend\": \"%s\"}", on_off_state(oper_suspended));
    prase->resp_handler(prase->presp);
    DBG_PRINTF1("operations suspension is now: %s\n", on_off_state(oper_suspended));
}

/*
 * Reset or factory reset
 *
 * curl -v "http://192.168.2.1/reset&factory=on"
 */
void api_reset_handler(Rase *prase, char *resp_body) {
    bool conf_reset;

    conf_reset = rase_get_value_as_bool_alias(prase, "factory");
    DBG_PRINT("Perform a reboot now\n");
    if (conf_reset) {
        DBG_PRINT("Reset configuration factory state\n");
        factory_reset(true);
    }
    else {
        reset_req = true;
    }
    prase->resp_handler("{}");
}

static BoolAlias balias[] = { { "yes", true }, { "no", false }, { "on", true }, { "off", false }, { "once", true },{ "repeat", false }, NULL };
static const char *pp1[] = { "channel", "state", "cexpr", "dur", "repeat" };
static const char *pp2[] = { "channel", "value" };
static const char *pp3[] = { "mac", "ip", "gw", "dns", "netmask", "ntpserv" };
static const char *pp4[] = { "state" };
static const char *pp5[] = { "factory" };
static APICommand api_table[] = {
    { "info",       "get", 0,  NULL, api_info_handler     },
    { "cronset",    "get", 5,  pp1 , api_cronset_handler  },
    { "set",        "get", 2,  pp1 , api_set_handler      },
    { "get",        "get", 1,  pp1 , api_get_handler      },
    { "aread",      "get", 1,  pp1 , api_aread_handler    },
    { "awrite",     "get", 2,  pp2 , api_awrite_handler   },
    { "getsched",   "get", 0,  NULL, api_getsched_handler },
    { "clear",      "get", 1,  pp1 , api_clear_handler    },
    { "config",     "get", 6,  pp3 , api_config_handler   },
    { "suspend",    "get", 1,  pp4 , api_suspend_handler  },
    { "reset",      "get", 1,  pp5 , api_reset_handler    },
    { NULL, NULL, 0, NULL, NULL }
};

/*
 * Show updated status through the serial port (used mainly for debugging)
 */
void report_status(void) {
    TimeChangeRule *tcr;
    int timers = Cron.count();
    time_t nn = now();
    int ix;

    time_t loct = CE.toLocal(now(), &tcr);

    CronEventClass *pat = Cron.get_alarm_table();
    DBG_PRINTF6("G:%02d:%02d:%02d L:%02d:%02d:%02d", hour(), minute(), second(), hour(loct), minute(loct), second(loct));
    if (timers > 0) {
        for (ix=0; ix<dtNBR_ALARMS; ix++) {
            if (pat->isEnabled) {
                DBG_PRINTF4("t=%02d @%02d:%02d:%02d ", ix, hour(pat->nextTrigger), minute(pat->nextTrigger), second(pat->nextTrigger));
                DBG_PRINTF4("d=%d p=%d s=%d r=%s ", sched_data[pat->cbdata-1].duration, pat->cbdata, sched_data[pat->cbdata-1].state, pat->isOneShot?"n":"y");
            }
            pat++;
        }
        DBG_PRINT("\n");
    }
    else {
        DBG_PRINT("\n");
    }
}

/*
 * send data back as response to HTTP REST API request
 *
 * presp: pointer to the response data
 */
int rase_resp_handler(char *presp) {
    return client.print(presp);
}

/*
 * Arduino loop
 */
void loop() {
    int index = 0;

    // read light value from LDS sensor
    //light_sensor_val = analogRead(LIGHT_SENSOR_PIN);
    //DBG_PRINT(light_sensor_val);

    if (timeStatus() != timeNotSet) {   // check if the time is successfully updated
        if (now() != prev_display) {       // update the display only if time has changed
            prev_display = now();
            // display the current date and time
            //DBG_PRINTF3("%04d-%02d-%02d", year(), month(), day());
            //DBG_PRINTF3(" %02d:%02d:%02d  ", hour(), minute(), second());
#ifdef _DEBUG_
            report_status();
#endif
        }
    }

    // listen for incoming clients
    client = server.available();
    if (client) {
        //  reset input buffer
        index = 0;
        while (client.connected()) {
            if (client.available()) {
                char c = client.read();
                //  fill url the buffer
                // Reads until either an eol character is reached or the buffer is full
                if (c != '\n' && c != '\r' && index < NETDATA_LEN) {
                    netbuf[index++] = c;
                    continue;
                }
                netbuf[index] = 0;
                // Flush any remaining bytes from the client buffer
                client.flush();
                rase_process_request(&rase, netbuf);
                break;
            }
        }

        // give the REST client some time to receive the data
        Cron.delay(3);
        // close the connection:
        client.stop();
    }
    Cron.delay();

    //check if any reset was preset with a specified number of cycles to wait before perform the reset
    if (reset_req) {
        reset_wait--;
        if (!reset_wait) {
            software_reset();
        }
    }
}

/*
 * Performs program initialization
 */
void setup() {
    int nx;

    DBG_INIT(57600);

    rase_init(&rase, api_table, balias, netbuf, NETDATA_LEN+1, rase_resp_handler);
    set_digital_pins(false);

    //factory_reset(false);
    EEPROM.get(0, config);
    show_config();
     // this is really needed with Arduino Nano Every
    Ethernet.init(10);
    //    dht.begin();
    // start the Ethernet connection and the server:
    Ethernet.begin(config.mac, config.ip, config.dns, config.gw, config.netmask);
    Serial.print("The DNS server IP address is: ");
    Serial.println(Ethernet.dnsServerIP());
    Serial.print("The gw IP address is: ");
    Serial.println(Ethernet.gatewayIP());

    DBG_PRINT("\nStarting NTP synchronization\n");
    ntp_init(config.ntpsrv, NTP_LOCAL_PORT);

    server.begin();
}
