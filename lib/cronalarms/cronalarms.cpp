/*
  CronAlarms.cpp - Arduino cron alarms
  Copyright (c) 2019 Martin Laclaustra

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  This is a wrapper of ccronexpr
  Copyright 2015, alex at staticlibs.net
  Licensed under the Apache License, Version 2.0
  https://github.com/staticlibs/ccronexpr

  API and implementation are inspired in TimeAlarms
  Copyright 2008-2011 Michael Margolis, maintainer:Paul Stoffregen
  GNU Lesser General Public License, Version 2.1 or later
  https://github.com/PaulStoffregen/TimeAlarms
 */

#include "cronalarms.h"

#include "debug.h"

extern "C" {
    #include "ccronexpr.h"
}


/* 
 * Cron Event Class Constructor
 */
CronEventClass::CronEventClass(void) {
    memset(&expr, 0, sizeof(expr));
    onTickHandler = NULL;  // prevent a callback until this pointer is explicitly set
    nextTrigger = 0;
    isEnabled = isOneShot = false;
}


/*
 * Cron Event Class Methods
 */
void CronEventClass::updateNextTrigger(void) {
    if (isEnabled) {
	time_t timenow = now();
	if (type == ALARMTYPE_CRON && onTickHandler != NULL && nextTrigger <= timenow) {
	    // update alarm if next trigger is not yet in the future
	    nextTrigger = cron_next(&expr, timenow);
	    DBG_PRINTF1("alarm updated to %lu\n", nextTrigger);
	}
    }
}


/* 
 * Cron Class Public Methods
 */

CronClass::CronClass(void) {
    isServicing = false;
    for(uint8_t id = 0; id < dtNBR_ALARMS; id++) {
	free(id);   
    }
}

void CronClass::enable(CronID_t ID) {
    if (isAllocated(ID)) {
	Alarm[ID].isEnabled = true;
	Alarm[ID].updateNextTrigger();
    }
}

void CronClass::disable(CronID_t ID) {
    if (isAllocated(ID)) {
	Alarm[ID].isEnabled = false;
    }
}

void CronClass::free(CronID_t ID) {
    if (isAllocated(ID)) {
	memset(&(Alarm[ID].expr), 0, sizeof(Alarm[ID].expr));
	Alarm[ID].onTickHandler = NULL;
	Alarm[ID].nextTrigger = 0;
	Alarm[ID].isEnabled = false;
	Alarm[ID].isOneShot = false;
	Alarm[ID].cbdata = 0;
    }
}

/*
 * returns the number of allocated timers
 */
uint8_t CronClass::count(void) const {
    uint8_t c = 0;
    for(uint8_t id = 0; id < dtNBR_ALARMS; id++) {
	if (isAllocated(id)) c++;
    }
    return c;
}

/*
 * returns true if the specified Alarm ID is in use
 */
bool CronClass::isAllocated(CronID_t ID) const {
    return (ID < dtNBR_ALARMS && Alarm[ID].onTickHandler != NULL);
}

/* 
 * get the currently triggered alarm id if any
 */
CronID_t CronClass::getTriggeredCronId() const {
    if (isServicing) {
	return servicedCronId;
    } else {
	return dtINVALID_ALARM_ID;
    }
}

/*
 * check if Cron Alarms is active
 */
bool CronClass::getIsServicing(void) const {
    return isServicing;
}

/*
 * Cron alarms service
 */
void CronClass::serviceAlarms()
{
    if (!isServicing) {
	isServicing = true;
	for (servicedCronId = 0; servicedCronId < dtNBR_ALARMS; servicedCronId++) {
	    if (Alarm[servicedCronId].isEnabled && (now() >= Alarm[servicedCronId].nextTrigger)) {
		cbHandler_t TickHandler = Alarm[servicedCronId].onTickHandler;
		if (TickHandler != NULL) {
		    DBG_PRINTF2(" calling tick handler alarm=%d cbdata=%d\n", servicedCronId, Alarm[servicedCronId].cbdata);
		    (*TickHandler)(Alarm[servicedCronId].cbdata);     // call the handler
		}
		if (Alarm[servicedCronId].isOneShot) {
		    free(servicedCronId);
		} else {
		    Alarm[servicedCronId].updateNextTrigger();
		}
	    }
	}
	isServicing = false;
    }
}

/*
 * get the absolute time of the next scheduled alarm
 */
time_t CronClass::getNextTrigger() const {
    time_t nextTrigger = 0;

    for (uint8_t id = 0; id < dtNBR_ALARMS; id++) {
	if (isAllocated(id)) {
	    if (nextTrigger == 0) {
		nextTrigger = Alarm[id].nextTrigger;
	    }
	    else if (Alarm[id].nextTrigger <  nextTrigger) {
		nextTrigger = Alarm[id].nextTrigger;
	    }
	}
    }
    return nextTrigger;
}

time_t CronClass::getNextTrigger(CronID_t ID) const {
    if (isAllocated(ID)) {
	return Alarm[ID].nextTrigger;
    } else {
	return 0;
    }
}

/*
 * Create a cron alarm and return CronID if successful
 */

CronID_t CronClass::create_cron(char *cronstring, cbHandler_t onTickHandler, bool isOneShot, int cbdata) {
    for (uint8_t id = 0; id < dtNBR_ALARMS; id++) {
	if (!isAllocated(id)) {
	    const char* err = NULL;
	    memset(&(Alarm[id].expr), 0, sizeof(Alarm[id].expr));
	    DBG_PRINTF1(" cronexpr=[%s]\n", cronstring);
	    cron_parse_expr(cronstring, &(Alarm[id].expr), &err);
	    if (err) {
		DBG_PRINTF1(" cronexpr error [%s]\n", err);
		memset(&(Alarm[id].expr), 0, sizeof(Alarm[id].expr));
		return dtINVALID_ALARM_ID;
	    }
	    Alarm[id].type = ALARMTYPE_CRON;
	    Alarm[id].onTickHandler = onTickHandler;
	    Alarm[id].isOneShot = isOneShot;
	    Alarm[id].cbdata = cbdata;
	    enable(id);
	    DBG_PRINTF2(" create time type=cron id=%d cbdata=%d\n", id, Alarm[id].cbdata);
	    return id;  // alarm created ok
	}
    }
    return dtINVALID_ALARM_ID; // no IDs available or time is invalid
}

/*
 * Create an alarm by a specifing numer of seconds from now
 */
CronID_t CronClass::create_alarm(time_t seconds, cbHandler_t onTickHandler, int cbdata) {

    for (uint8_t id = 0; id < dtNBR_ALARMS; id++) {
	if (!isAllocated(id)) {
	    Alarm[id].type = ALARMTYPE_SIMPLE;
	    Alarm[id].nextTrigger = seconds + now();
	    Alarm[id].onTickHandler = onTickHandler;
	    Alarm[id].isOneShot = true;
	    Alarm[id].cbdata = cbdata;
	    enable(id);
	    DBG_PRINTF2(" create timer type=alarm id=%d cbdata=%d\n", id, Alarm[id].cbdata);
	    return id;  // alarm created ok
	}
    }
    return dtINVALID_ALARM_ID; // no IDs available or time is invalid
}

int CronClass::dump_alarms(void) {
    for (uint8_t id = 0; id < dtNBR_ALARMS; id++) {
	DBG_PRINTF3(" -> id=%id cbdata=%d allocated=%lu\n", id, Alarm[id].cbdata, Alarm[id].onTickHandler);
    }
}


/*
 * Cron alarms dispatcher It myst called in the Arduino main loop or any long blocking routine/function 
 * to allow Cron alarm to work properly
 */
void CronClass::delay(unsigned long ms) {
    unsigned long start = millis();
    do {
	serviceAlarms();
	yield();
    } while (millis() - start  <= ms);
}


void CronClass::checkAlarms() {
    int ix;
    time_t nt;
    time_t nn = now();

    DBG_PRINTF3("%02d:%02d:%02d ", hour(nn), minute(nn), second(nn));
    for (ix = 0; ix < dtNBR_ALARMS; ix++) {
	if (Alarm[ix].isEnabled) {
	    nt = Alarm[ix].nextTrigger;
	    DBG_PRINTF4("t=%02d @%02d:%02d:%02d ", ix, hour(nt), minute(nt), second(nt));
	}
    }
    DBG_PRINT("\n");
}

CronEventClass *CronClass::get_alarm_table(void) const {
    return Alarm;
}


// make one instance for the user to use
CronClass Cron = CronClass();
