/*
 netrelay: Arduino network controlled relay board
 useful macro for debugging needs.

 Copyright (c) 2022 Franco Fiorese <franco@f2hex.net>

 first code date: 26-mar-2017
 updated: 25-aug-2022
*/

#ifndef _DEBUG_H_
#define _DEBUG_H_

#define DBG_BUF_SIZE 128

#if _DEBUG_
    static char dbg_buf[DBG_BUF_SIZE+1];
    #ifdef _ARDUINO_
        #define DBG_INIT(v)                          Serial.begin(v);
        #define DBG_PRINT(v)                         Serial.print(v)
        #define DBG_PRINTF1(f,v1)                    snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1);Serial.print(dbg_buf);
        #define DBG_PRINTF2(f,v1,v2)                 snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1,v2);Serial.print(dbg_buf);
        #define DBG_PRINTF3(f,v1,v2,v3)              snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1,v2,v3);Serial.print(dbg_buf);
        #define DBG_PRINTF4(f,v1,v2,v3,v4)           snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1,v2,v3,v4);Serial.print(dbg_buf);
        #define DBG_PRINTF5(f,v1,v2,v3,v4,v5)        snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1,v2,v3,v4,v5);Serial.print(dbg_buf);
        #define DBG_PRINTF6(f,v1,v2,v3,v4,v5,v6)     snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1,v2,v3,v4,v5,v6);Serial.print(dbg_buf);
        #define DBG_PRINTF7(f,v1,v2,v3,v4,v5,v6,v7)  snprintf(dbg_buf,DBG_BUF_SIZE+1,f,v1,v2,v3,v4,v5,v6,v7);Serial.print(dbg_buf);
    #else
        #define DBG_INIT(v)
        #define DBG_PRINT(v)                         printf(v)
        #define DBG_PRINTF1(f,v1)                    printf(f,v1)
        #define DBG_PRINTF2(f,v1,v2)                 printf(f,v1,v2)
        #define DBG_PRINTF3(f,v1,v2,v3)              printf(f,v1,v2,v3)
        #define DBG_PRINTF4(f,v1,v2,v3,v4)           printf(f,v1,v2,v3,v4)
        #define DBG_PRINTF5(f,v1,v2,v3,v4,v5)        printf(f,v1,v2,v3,v4,v5)
        #define DBG_PRINTF6(f,v1,v2,v3,v4,v5,v6)     printf(f,v1,v2,v3,v4,v5,v6)
        #define DBG_PRINTF7(f,v1,v2,v3,v4,v5,v6,v7)  printf(f,v1,v2,v3,v4,v5,v6,v7)
    #endif
#else
    #define DBG_INIT(v)
    #define DBG_PRINT(v)
    #define DBG_PRINTF1(f,v1)
    #define DBG_PRINTF2(f,v1,v2)
    #define DBG_PRINTF3(f,v1,v2,v3)
    #define DBG_PRINTF4(f,v1,v2,v3,v4)
    #define DBG_PRINTF5(f,v1,v2,v3,v4,v5)
    #define DBG_PRINTF6(f,v1,v2,v3,v4,v5,v6)
    #define DBG_PRINTF7(f,v1,v2,v3,v4,v5,v7)
#endif

#endif /* _DEBUG_H_ */
