/*
  RESTful API Server Engine - for embedded system

  Copyright (c) 2022 Franco Fiorese <franco@f2hex.net>

  first code date: 26-mar-2017
  updated: 25-aug-2022
*/

#ifndef _RASE_H_
#define _RASE_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "debug.h"

#define MAX_KEYVALS 10
#define MAX_METHOD_LEN 7
#define MAX_COMMAND_LEN 10
#define MAX_KEYVAL_LEN  24
#define MAX_HTTPVER_LEN 12

typedef enum parser_result {
    RASE_RES_SUCCESS      = 0,
    RASE_RES_INVCOMMAND   = 1,
    RASE_RES_INVARGSNUM   = 2,
    RASE_RES_INVARGSNAME  = 3,
    RASE_RES_INVMETHOD    = 4,
    RASE_RES_OVERFLOW     = 5,
    RASE_RES_MALFORMEDREQ = 6,
} RaseResult;

struct apicommand;

typedef struct bool_alias {
    const char *txt;
    bool state;
} BoolAlias;

typedef int (*RASE_Resp_Handler)(char *presp);

typedef struct rase {
    char http_ver[MAX_HTTPVER_LEN+1];
    char method[MAX_METHOD_LEN+1];
    char cmd[MAX_COMMAND_LEN+1];
    char keys[MAX_KEYVALS][MAX_KEYVAL_LEN + 1];
    char vals[MAX_KEYVALS][MAX_KEYVAL_LEN + 1];
    BoolAlias *pbalias;
    struct apicommand *api_cmds;
    RASE_Resp_Handler resp_handler;
    char *presp;
    int resp_size;
    int scode;
} Rase;

// API handler callback function typedef
typedef void (*RASE_API_Handler)(Rase *prase);

// structure to hold API command info
typedef struct apicommand {
    const char *cmd;
    const char *method;
    int args;
    const char **parms;
    RASE_API_Handler api_handler;
} APICommand;

typedef struct http_status {
    int status;
    const char *preason;
} HTTPStatus;


void rase_init(Rase *prase, APICommand *apicmds, BoolAlias *pbalias, char *presp, int resp_size, RASE_Resp_Handler resph);
void rase_reset(Rase *prase);
int rase_process_request(Rase *prase, char *pcmd);
void rase_dump_table(Rase *prase);
bool rase_get_bool_alias(Rase *prase, const char *pkey);
const char *rase_get_value_as_string(Rase *prase, const char *pkey);
int rase_get_value_as_int(Rase *prase, const char *pkey);
long rase_get_value_as_long(Rase *prase, const char *pkey);
bool rase_get_value_as_bool_alias(Rase *prase, const char *pkey);
double rase_get_value_as_floati(Rase *prase, const char *pkey);

#endif /*_RASE_H_ */
