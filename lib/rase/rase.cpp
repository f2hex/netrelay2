/*
  RESTful API Server Engine - for embedded system

  Copyright (c) 2022 Franco Fiorese <franco@f2hex.net>

  first code date: 26-mar-2017
  updated: 25-aug-2022
*/

#include <stdio.h>
#include <string.h>

#include "rase.h"

#define streq(a,b) !strcmp(a,b)

static const char *http_hdr = "HTTP/1.1";
static const HTTPStatus http_sts[] = {
    { 200, "OK" },
    { 400, "Bad Request" },
    { 405, "Method not allowed" },
    { -1,  NULL }
};

static const char *pmsg[] = {
    "success",
    "invalid command",
    "invalid number of arguments",
    "invalid parameter names",
    "invalid HTTP method",
    "command buffer overflow",
    "malformed request"
};

/*
 * Resets parser engine
 */
void rase_reset(Rase *prase) {
    char *pck = prase->keys[0];
    char *pcv = prase->vals[0];
    int cx = 0;
    for (cx = 0; cx < MAX_KEYVALS; cx++) {
        memset(pck, 0, MAX_KEYVAL_LEN+1);
        memset(pcv, 0, MAX_KEYVAL_LEN+1);
        pck += (MAX_KEYVAL_LEN + 1);
        pcv += (MAX_KEYVAL_LEN + 1);
    }
    prase->scode = 200;
}

void rase_init(Rase *prase, APICommand *apicmds, BoolAlias *pbalias, char *presp, int resp_size, RASE_Resp_Handler resph) {
    prase->api_cmds = apicmds;
    prase->pbalias = pbalias;
    prase->presp = presp;
    prase->resp_handler = resph;
    prase->resp_size = resp_size;
    rase_reset(prase);
}

void rase_dump_table(Rase *prase) {
    char *pkeys = prase->keys[0];
    char *pvals = prase->vals[0];
    int cx = 0;

    DBG_PRINT("---\n");
    DBG_PRINTF1("command: [%s]\n", prase->cmd);
    for (cx = 0; cx < MAX_KEYVALS; cx++) {
        //if (*pkeys) {
            DBG_PRINTF2("%02d: [%s]=", cx, pkeys);
            if (*pvals) {
                DBG_PRINTF1("[%s]\n", pvals);
            }
            else {
                DBG_PRINT("\n");
            }
            //}
        pkeys += (MAX_KEYVAL_LEN + 1);
        pvals += (MAX_KEYVAL_LEN + 1);
    }
    DBG_PRINT("---\n");
}

const char *rase_getval(Rase *prase, const char *pkey) {
    int sl = strlen(pkey);
    int nx;

    for (nx=0; nx<MAX_KEYVALS; nx++) {
        if (strncmp(pkey, prase->keys[nx], sl) == 0) {
            return prase->vals[nx];
        }
    }
    return "";
}


/*
 * Returns the boolean value by looking up values in a boolean alias words.
 */
bool rase_get_bool_alias(Rase *prase, const char *pkey) {
    BoolAlias *pbalias = prase->pbalias;

    while (pbalias->txt != NULL) {
        if (streq(pbalias->txt, pkey)) {
            return pbalias->state;
        }
        pbalias++;
    }
    return false;
}

/*
 * Return the string value of the token at the specified index.
 */
const char *rase_get_value_as_string(Rase *prase, const char *pkey) {
    return rase_getval(prase, pkey);;
}

/*
 * Return the integer value of the token at the specified index.
 * Special case: if the token is a '*' -1 will be returned.
 */
int rase_get_value_as_int(Rase *prase, const char *pkey) {
    const char *pval = rase_getval(prase, pkey);
    if (streq(pval, "*")) {
        return -1;
    }
    else {
        return atoi(pval);
    }
}

/*
 * Return the long value of the token at the specified index.
 */
long rase_get_value_as_long(Rase *prase, const char *pkey) {
    return atol(rase_getval(prase, pkey));
}

/*
 * Return the float value of the token at the specified index.
 */
double rase_get_value_as_float(Rase *prase, const char *pkey) {
    return atof(rase_getval(prase, pkey));
}

/*
 * Return the boolean value of the token at the specified index.
 */
bool rase_get_value_as_bool_alias(Rase *prase, const char *pkey) {
    return rase_get_bool_alias(prase, rase_getval(prase, pkey));
}

/*
 * Copy the specified token into the destination buffer
 *
 * pdest: destination buffer pointer
 * ptoken: point to the token string (not zero-terminated)
 * len: length of the string
 * allowed_len: max destination buffer length
 *
 * return: false if successful else true if budder overflow
 */
static bool _save_token(char *pdest, char *ptoken, int len, int allowed_len) {
    if (len <= allowed_len) {
        memcpy(pdest, ptoken, len);
        *(pdest+len) = '\0';
        return false;
    }
    else {
        return true;
    }
}

static bool _check_parm_name(const char **pparms, char *parm) {
    while (*pparms) {
        if (streq(*pparms, parm)) {
            return true;
        }
        pparms++;
    }
    return false;
}


static const char *_get_http_sts_reason(int status) {
    HTTPStatus const *psr = http_sts;

    while (psr->status != -1) {
        if (status == psr->status) {
            return psr->preason;
        }
        psr++;
    }
    return "Unknown";
}

/*
 * HTTP Request parser engine
 *
 * The standard augmented BNF Syntax is
 *
 *  Request-Line = Method SP Request-URI SP HTTP-Version CRLF
 *  typical example:
 *
 *  GET /set?chan=*&state=on HTTP/1.1
 *  prase: pointer to AParse instance data
 *  pcmd: pointer to the AParse API command table
 *
 */
int rase_process_request(Rase *prase, char *preq) {
    APICommand *pcmds = prase->api_cmds;
    char *pc;
    int res;
    int kv_cnt;
    char *ptoken;
    int len;
    char *pck;
    char *pcv;
    const char **pparm;
    int found;
    int nx;

    kv_cnt = 0;
    res = RASE_RES_SUCCESS;
    pc = preq;

    // STEP 1: get HTTP method
    // skip any leading space
    while (*pc == ' ' && *pc != '\0') {
        pc++;
    }
    ptoken = pc;
    // fetch the method name
    while (*pc != ' ' && *pc != '\0') {
        pc++;
    }
    if (_save_token(prase->method, ptoken, pc - ptoken, MAX_METHOD_LEN)) {
        res = RASE_RES_MALFORMEDREQ;
    }
    else {
        // STEP 2: fetch API command name
        // skip past spaces
        while (*pc == ' ' && *pc != '\0') {
            pc++;
        }
        // skip leading slash '/'
        while (*pc == '/' && *pc != '\0') {
            pc++;
        }
        ptoken = pc;
        // fetch the command name
        while (*pc != '?' && *pc != ' ' && *pc != '\0') {
            pc++;
        }
        if (*pc == '\0' || _save_token(prase->cmd, ptoken, pc - ptoken, MAX_COMMAND_LEN)) {
            res = RASE_RES_MALFORMEDREQ;
        }
        else {
	    // chek for params prefix
            if (*pc == '?') {
                pc++;
                // STEP 3: parse command parameters and populate key/value table
                pck = prase->keys[0];
                pcv = prase->vals[0];
                while (*pc) {
                    ptoken = pc;
                    while (*pc != '=' && *pc != '\0' && *pc != ' ') {
                        pc++;
                    }
                    if (*pc == '\0') {
                        // incomplete data: key is missing
                        res = RASE_RES_MALFORMEDREQ;
			break;
                    }
                    // save parameter key
                    if (_save_token(pck, ptoken, pc - ptoken, MAX_KEYVAL_LEN)) {
                        res = RASE_RES_MALFORMEDREQ;
			break;
                    }
                    if (*pc == '\0') {
                        // incomplete data: value is missing
                        res = RASE_RES_MALFORMEDREQ;
			break;
                    }
                    pc++;
                    ptoken = pc;
                    while (*pc != '&' && *pc != ' ' && *pc != '\0') {
                        pc++;
                    }
                    if (*pc == '\0') {
                        // incomplete data: value is missing
                        res = RASE_RES_MALFORMEDREQ;
			break;
                    }

                    // save parameter value
                    if (_save_token(pcv, ptoken, pc - ptoken, MAX_KEYVAL_LEN)) {
                        res = RASE_RES_MALFORMEDREQ;
			break;
                    }
                    kv_cnt++;
                    if (*pc == ' ') {
                        // end of Request-URI
                        break;
                    }
                    // move to next key/value slot
                    pck = pck + (MAX_KEYVAL_LEN + 1);
                    pcv = pcv + (MAX_KEYVAL_LEN + 1);
                    pc++;
                }
	    }
        }

	if (res == RASE_RES_SUCCESS) {
	    // STEP 4: look for HTTP version token
	    // skip futher spaces
	    while (*pc  == ' ' && *pc != '\0') {
                pc++;
	    }
	    ptoken = pc;
	    while (*pc != '\r' && *pc != '\0') {
                pc++;
	    }
	    if (_save_token(prase->http_ver, ptoken, pc - ptoken, MAX_HTTPVER_LEN)) {
                res = RASE_RES_MALFORMEDREQ;
	    }
	}
    }


    if (res == RASE_RES_SUCCESS) {
        // check if method and API command are supported
        pcmds = prase->api_cmds;
        while (pcmds->cmd != NULL) {
            if (streq(pcmds->cmd, prase->cmd)) {
                if (strncasecmp(pcmds->method, prase->method, strlen(pcmds->method)) != 0 ) {
                    res = RASE_RES_INVMETHOD;
                        prase->scode = 405;
                }
                else if (pcmds->args != kv_cnt) {
                    res = RASE_RES_INVARGSNUM;
                        prase->scode = 400;
                }
                else {
                    // validate argument names
                    for (nx=0; nx<kv_cnt; nx++) {
                        if (!_check_parm_name(pcmds->parms, prase->keys[nx])) {
                            res = RASE_RES_INVARGSNAME;
                            prase->scode = 400;
                            break;
                        }
                    }
                }
                break;
            }
            pcmds++;
        }
    }
    if (pcmds->cmd == NULL) {
        res = RASE_RES_INVCOMMAND;
        prase->scode = 400;
    }
    rase_dump_table(prase);
    // send response header
    snprintf(prase->presp, prase->resp_size, "%s %d %s\r\n", http_hdr, prase->scode, _get_http_sts_reason(prase->scode));
    prase->resp_handler(prase->presp);
    prase->resp_handler("Content-Type: application/json\r\n\r\n{\"data\": ");
    if (res == RASE_RES_SUCCESS) {
        // call the API command handler
        pcmds->api_handler(prase);
    }
    else {
	prase->resp_handler("{}");
    }
    snprintf(prase->presp, prase->resp_size, ", \"result\": \"%s\"}\r\n", pmsg[res]);
    prase->resp_handler(prase->presp);
    DBG_PRINTF2("res=%d %s\n", res, pmsg[res]);

    return res;
}
