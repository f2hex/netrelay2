#ifndef _NTPLIB_H_
#define _NTPLIB_H_

#include <Arduino.h>
#include <TimeLib.h>
#include <EthernetENC.h>
#include <debug.h>

void ntp_init(char *pntpsrv, unsigned int port);
time_t ntp_get_time(void);
void ntp_send_req(IPAddress &address);

#endif

