/*
 * A very bare NTP clint.
 * Code taken from https://www.arduino.cc/en/Tutorial/LibraryExamples/UdpNtpClient
 */

#include "ntpclient.h"
#include "Dns.h"

#define NTP_MSGBUF_LEN 48

#define _DEBUG_ true

// define Ethernet UDP object and local port 8888
EthernetUDP ethernet_UDP;
// array to hold incoming/outgoing NTP messages
// NTP time message is 48 bytes long
byte msg_buf[NTP_MSGBUF_LEN];
IPAddress ntp_server_ip;
DNSClient mdns;

time_t ntp_get_time() {
    while (ethernet_UDP.parsePacket() > 0) ; // discard packets remaining to be parsed

    DBG_PRINT("Transmit NTP Request message\n");
    // send packet to request time from NTP server
    ntp_send_req(ntp_server_ip);

    // wait for response
    uint32_t beginWait = millis();

    while (millis() - beginWait < 1500) {

        int size = ethernet_UDP.parsePacket();

        if (size >= NTP_MSGBUF_LEN) {
            DBG_PRINT("Receiving NTP Response\n");

            // read data and save to msg_buf
            ethernet_UDP.read(msg_buf, NTP_MSGBUF_LEN);

            // NTP time received will be the seconds elapsed since 1 January 1900
            unsigned long secsSince1900;

            // convert to an unsigned long integer the reference timestamp found at byte 40 to 43
            secsSince1900 =  (unsigned long)msg_buf[40] << 24;
            secsSince1900 |= (unsigned long)msg_buf[41] << 16;
            secsSince1900 |= (unsigned long)msg_buf[42] << 8;
            secsSince1900 |= (unsigned long)msg_buf[43];
            
            // returns UTC time
          return secsSince1900 - 2208988800UL;
        }
    }
  
    // error if no response
    DBG_PRINT("Error: No Response.\n");
    return 0;
}

/*
   helper function for getTime()
   this function sends a request packet 48 bytes long
*/
void ntp_send_req(IPAddress &address) {
    // set all bytes in msg_buf to 0
    memset(msg_buf, 0, NTP_MSGBUF_LEN);

    // create the NTP request message

    msg_buf[0] = 0b11100011;  // LI, Version, Mode
    msg_buf[1] = 0;           // Stratum, or type of clock
    msg_buf[2] = 6;           // Polling Interval
    msg_buf[3] = 0xEC;        // Peer Clock Precision
    // array index 4 to 11 is left unchanged - 8 bytes of zero for Root Delay & Root Dispersion
    msg_buf[12]  = 49;
    msg_buf[13]  = 0x4E;
    msg_buf[14]  = 49;
    msg_buf[15]  = 52;
    
    // send msg_buf to NTP server via UDP at port 123
    ethernet_UDP.beginPacket(address, 123);
    ethernet_UDP.write(msg_buf, NTP_MSGBUF_LEN);
    ethernet_UDP.endPacket();
}

void ntp_init(char *pntpsrv, unsigned int port) {
    ethernet_UDP.begin(port);
    
    mdns.begin(Ethernet.dnsServerIP());
    int ret = mdns.getHostByName(pntpsrv, ntp_server_ip);
    DBG_PRINTF2("host: %s ret: %d  NTP server ip: ", config.ntpsrv, ret);
    DBG_PRINT(ntp_server_ip);
    DBG_PRINT("\n");
    
    setSyncProvider(ntp_get_time);
}

